package com.gft.agencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
public class AgenciaMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgenciaMsApplication.class, args);
		System.out.println("running app");
	}


}
