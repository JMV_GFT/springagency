package com.gft.agencia.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gft.agencia.business.IAgenciaService;
import com.gft.agencia.dao.model.Car;
import com.gft.agencia.dao.repository.IAgencyRepo;

@Service
public class AgenciaServiceImpl implements IAgenciaService{

	@Autowired
	private IAgencyRepo agenciaDao;
	
	@Override
	public List<Car> getCars() {
		return agenciaDao.findAll();
	}

	@Override
	public Optional<Car> getCar(int id) {
		return agenciaDao.findById(id);
	}

	@Override
	public void deleteCar(int id) {
		agenciaDao.deleteById(id);
	}

	@Override
	public void putCar(int id, Car c) {
		Optional<Car> c1 = agenciaDao.findById(id);
		Car c2 = c1.get();
		
		c2.setModel(c.getModel());
		c2.setBrand(c.getBrand());
		c2.setPrice(c.getPrice());
		c2.setType(c.getType());
		
		agenciaDao.save(c2);		
	}

	@Override
	public void patchCar(int id, Car c) {
		Optional<Car> c1 = agenciaDao.findById(id);
		Car c2 = c1.get();

		if(c.getModel()==null | c.getModel()=="") {
			c2.setModel(c2.getModel());
		}else {
			c2.setModel(c.getModel());
		}
		if(c.getBrand()==null | c.getBrand() == "") {
			c2.setBrand(c2.getBrand());
		}else {
			c2.setBrand(c.getBrand());
		}
		if(c.getType()==null | c.getType() == "") {
			c2.setType(c2.getType());
		}else {
			c2.setType(c.getType());
		}
		if(c.getPrice() == null | c.getPrice()==0) {
			c2.setPrice(c2.getPrice());
		}else {
			c2.setPrice(c.getPrice());
		}
		agenciaDao.save(c2);		
	}

	@Override
	public Integer saveCar(Car c) {
		agenciaDao.save(c);
		return c.getId();
	}

}
