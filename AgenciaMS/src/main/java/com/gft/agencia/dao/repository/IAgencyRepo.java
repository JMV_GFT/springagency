package com.gft.agencia.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gft.agencia.dao.model.Car;

@Repository
public interface IAgencyRepo extends JpaRepository<Car, Integer>{

}

 