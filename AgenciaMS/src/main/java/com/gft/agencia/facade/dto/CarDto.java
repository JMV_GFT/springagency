package com.gft.agencia.facade.dto;

public class CarDto {
	private Integer id;
	private String type;
	private String brand;
	private String model;
	private Float price;

}
