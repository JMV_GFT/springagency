package com.gft.agencia.facade.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gft.agencia.business.IAgenciaService;
import com.gft.agencia.dao.model.Car;

@RestController
@RequestMapping("/api")
public class RestAgenciaController {
	
	@Autowired
	IAgenciaService agenciaService;
	
	@GetMapping
	@RequestMapping("/get/{id}")
	public Optional<Car> getCar(@PathVariable(name = "id") int id){
		return agenciaService.getCar(id);
	}
	
	@GetMapping
	@RequestMapping("/gets/{id}")
	public List<Car> getCars(@PathVariable(name = "id")int id){
		return agenciaService.getCars();
	}
	
	@PostMapping
	public int getCustomer(@RequestBody Car c) {
		return agenciaService.saveCar(c);
	}
	
	@PutMapping("/{id}")
	public void putCar(@PathVariable(name = "id") int id, @RequestBody Car c) {
		agenciaService.putCar(id, c);
	}
	
	@PatchMapping("/{id}")
	public void patchCar(@PathVariable(name = "id") int id, @RequestBody Car c) {
		agenciaService.patchCar(id, c);
	}
	
	
	
}
