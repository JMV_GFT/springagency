package com.gft.agencia.business;

import java.util.List;
import java.util.Optional;

import com.gft.agencia.dao.model.Car;

public interface IAgenciaService {
	List<Car> getCars();
	Optional<Car> getCar(int id);
	void deleteCar(int id);
	void putCar(int id, Car c);
	void patchCar(int id, Car c);
	Integer saveCar(Car c);
}
